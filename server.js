
var express = require("express");
var bodyParser = require("body-parser");
var nodemailer = require("nodemailer");
var sesTransport = require("nodemailer-ses-transport");
var passport = require("passport");
var FacebookStrategy = require('passport-facebook').Strategy;
var session = require('express-session');
var cookieParser = require('cookie-parser');

var FACEBOOK_APP_ID = '1016354755115578';
var FACEBOOK_APP_SECRET = 'f90cc3218812fdd2f21dde4cf9cc671d';
var app = express();

app.use(session({ secret: 'keyboard cat', key: 'sid'}));

var PORT = process.env.PORT || 3000;

passport.serializeUser(function(user, done) {
    done(null, user);
});
passport.deserializeUser(function(obj, done) {
    done(null, obj);
});

passport.use(new FacebookStrategy({
        clientID: FACEBOOK_APP_ID,
        clientSecret: FACEBOOK_APP_SECRET ,
        callbackURL: 'http://localhost:3000/auth/facebook/callback'
    },
    function(accessToken, refreshToken, profile, cb) {
    }
));

var tranport = nodemailer.createTransport({
    transport: "ses",
    accessKeyId: process.env.ACCESSKEYID,
    secretAccessKey: process.env.SECRETACCESSKEY
});


app.use('/', express.static("static"));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());
app.use(cookieParser())
app.use(session());

function sendEmail(subject, content, receiver, cb) {
    var mailData = {
        from: process.env.FROM_ADDRESS,
        to: receiver,
        subject: subject,
        text: content
    };
    tranport.sendMail(mailData, function (err, info) {
        console.log(err);
        console.log(info);
        if(err){
            cb(400);
        }
        else{
            cb(200);
        }
    });
}

app.get('/', function(req, res){
    if(!req.session.authenticated){
        res.sendFile(__dirname + '/static/index.html');
    }
    else{
        res.redirect('/send-email');
    }
});

app.get('/send-mail', function(req, res){
    if(req.session.authenticated){
        res.sendFile(__dirname + '/static/send-page.html');
    }
    else{
        res.redirect('/');
    }
});


app.post("/email", function (req, res) {
    var content = req.body.content;
    var subject = req.body.subject;
    var receiver = req.body.reciever;
    if(!content || !subject || !receiver){
        return res.sendStatus(400)
    }
    status_email = sendEmail(subject, content, receiver, function (status) {
        return res.sendStatus(status);
    });
});

app.get('/auth/facebook', passport.authenticate('facebook'));
app.get('/auth/facebook/callback', function(req, res) {
    req.session.authenticated = true
    res.redirect('/send-mail');
    });


app.listen(PORT, function () {
    console.log("expample app listenning on port http://localhost:" + PORT);
});

